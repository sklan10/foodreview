"""geosite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.flatpages import views

import locations.views
import profiles.views
import locations.serializers
import profiles.serializers

from rest_framework import routers
router = routers.DefaultRouter()
router.register(r'users', locations.serializers.UserViewSet)
router.register(r'user_profiles', locations.serializers.UserProfileViewSet)
router.register(r'location_tags', locations.serializers.LocationTagViewSet)
router.register(r'location_hours', locations.serializers.LocationHoursViewSet)
router.register(r'menu_items', locations.serializers.MenuItemViewSet)
router.register(r'item_ratings', locations.serializers.ItemRatingViewSet)
router.register(r'locations_reviews', locations.serializers.LocationReviewViewSet)
router.register(r'location_images', locations.serializers.LocationImageViewSet)
router.register(r'locations', locations.serializers.LocationViewSet)

admin.autodiscover()
admin.site.site_header = 'GrubYea'

urlpatterns = [
    url(r'^$', locations.views.home, name='home'),
    url(r'^search/$', locations.views.search, name="search"),
    url(r'^location/(?P<slug>[\w-]+)/$', locations.views.location, name="location"),
    url(r'^location/review/(?P<slug>[\w-]+)/$', locations.views.write_review, name="write_review"),
    url(r'^location/add_favorite/(?P<slug>[\w-]+)/$', locations.views.add_favorite, name="add_favorite"),

    url(r'^profile/', profiles.views.profile, name="profile"),
    url(r'^user_login/$', profiles.views.user_login, name="user_login"),
    url(r'^user_register/$', profiles.views.user_register, name="user_register"),
    url(r'^user_logout/$', profiles.views.user_logout, name="user_logout"),
    url(r'^register_confirm/(?P<activation_key>[\w]+)/$', profiles.views.register_confirm, name='register_confirm'),

    url(r'^password_reset/$', auth_views.password_reset, name="password_reset"),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),

    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    
    url(r'^admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

urlpatterns += [
    url(r'^(?P<url>.*/)$', views.flatpage),
]
