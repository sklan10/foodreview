from django.contrib import admin

from .models import Location, LocationHours, MenuItem, ItemRating, LocationReview, LocationImage

class LocationHoursInline(admin.TabularInline):
	model = LocationHours
	extra = 0

class MenuItemInline(admin.TabularInline):
	model = MenuItem
	extra = 0

class ItemRatingInline(admin.TabularInline):
	model = ItemRating
	extra = 0

class LocationReviewInline(admin.TabularInline):
	model = LocationReview
	extra = 0

class LocationImageInline(admin.TabularInline):
	model = LocationImage
	extra = 0

class LocationAdmin(admin.ModelAdmin):
	inlines = [LocationHoursInline, MenuItemInline, ItemRatingInline, LocationReviewInline, LocationImageInline]
	search_fields = ['name', 'city', 'state', 'zip_code']
	list_display = ['name', 'address', 'city', 'state', 'zip_code', 'slug', 'featured']
	list_filter = ['state', 'price']
	list_editable = ('featured',)
	prepopulated_fields = {"slug": ("name",)}
	
	class Meta:
		model = Location

	def get_queryset(self, request):
		my_model = super(LocationAdmin, self).get_queryset(request)
		my_model = my_model.prefetch_related('locationhours_set', 'menuitem_set', 'itemrating_set', 'locationreview_set', 'locationimage_set')
		return my_model

admin.site.register(Location, LocationAdmin)