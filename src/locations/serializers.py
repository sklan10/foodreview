from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets

from locations.models import LocationTag, Location, LocationHours, MenuItem, ItemRating, LocationReview, LocationImage, FavoriteLocation
from profiles.models import UserProfile


# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'is_staff', )

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer



# Serializers define the API representation.
class UserProfileSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()
    class Meta:
        model = UserProfile
        fields = ('user', 'location', 'description', 'image', 'activation_key', 'key_expires', 'timestamp', 'updated')

# ViewSets define the view behavior.
class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer



# Serializers define the API representation.
class LocationTagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = LocationTag
        fields = ('tag', 'timestamp',)

# ViewSets define the view behavior.
class LocationTagViewSet(viewsets.ModelViewSet):
    queryset = LocationTag.objects.all()
    serializer_class = LocationTagSerializer



# Serializers define the API representation.
class LocationHoursSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = LocationHours
        fields = ('day', 'open_time', 'close_time', 'timestamp', 'updated',)

# ViewSets define the view behavior.
class LocationHoursViewSet(viewsets.ModelViewSet):
    queryset = LocationHours.objects.all()
    serializer_class = LocationHoursSerializer



# Serializers define the API representation.
class MenuItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MenuItem
        fields = ('name', 'description', 'price', 'category', 'image', 'timestamp', 'updated',)

# ViewSets define the view behavior.
class MenuItemViewSet(viewsets.ModelViewSet):
    queryset = MenuItem.objects.all()
    serializer_class = MenuItemSerializer



# Serializers define the API representation.
class ItemRatingSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()
    class Meta:
        model = ItemRating
        fields = ('user', 'item', 'name', 'description', 'rating', 'timestamp', 'updated',)

# ViewSets define the view behavior.
class ItemRatingViewSet(viewsets.ModelViewSet):
    queryset = ItemRating.objects.all()
    serializer_class = ItemRatingSerializer



# Serializers define the API representation.
class LocationReviewSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()
    class Meta:
        model = LocationReview
        fields = ('user', 'review', 'featured', 'timestamp', 'updated',)

# ViewSets define the view behavior.
class LocationReviewViewSet(viewsets.ModelViewSet):
    queryset = LocationReview.objects.all()
    serializer_class = LocationReviewSerializer



# Serializers define the API representation.
class LocationImageSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()
    class Meta:
        model = LocationImage
        fields = ('user', 'review', 'image', 'image_str', 'attributions', 'default', 'featured', 'timestamp', 'updated',)

# ViewSets define the view behavior.
class LocationImageViewSet(viewsets.ModelViewSet):
    queryset = LocationImage.objects.all()
    serializer_class = LocationImageSerializer



# Serializers define the API representation.
class FavoriteLocationSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()
    class Meta:
        model = FavoriteLocation
        fields = ('user', 'location', 'active', 'timestamp', 'updated',)

# ViewSets define the view behavior.
class FavoriteLocationViewSet(viewsets.ModelViewSet):
    queryset = FavoriteLocation.objects.all()
    serializer_class = FavoriteLocationSerializer



# Serializers define the API representation.
class LocationSerializer(serializers.HyperlinkedModelSerializer):
    locationhours_set = LocationHoursSerializer(many=True)
    menuitem_set = MenuItemSerializer(many=True)
    itemrating_set = ItemRatingSerializer(many=True)
    locationreview_set = LocationReviewSerializer(many=True)
    locationimage_set = LocationImageSerializer(many=True)

    class Meta:
        model = Location
        fields = ('name', 'address', 'city', 'state', 'zip_code', 'phone', 'website', 'menu_internal', 'menu_external', 'longitude', 'latitude', 'slug', 'tag', 'price', 'link', 'featured', 'food_rating', 'restaurant_rating', 'timestamp', 'updated', 'locationhours_set', 'menuitem_set', 'itemrating_set', 'locationreview_set', 'locationimage_set')

    @staticmethod
    def setup_eager_loading(queryset):
        """ Perform necessary eager loading of data. """
        # prefetch_related for "to-many" relationships
        queryset = queryset.prefetch_related('locationhours_set', 'menuitem_set', 'itemrating_set', 'locationreview_set', 'locationimage_set')
        return queryset

# ViewSets define the view behavior.
class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

