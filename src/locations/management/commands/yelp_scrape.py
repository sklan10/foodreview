import os
import re
import requests
import urllib2
from decimal import *
import translitcodec
import codecs

import googlemaps
from bs4 import BeautifulSoup
from geopy.geocoders import Nominatim
#from opencage.geocoder import OpenCageGeocode

from django.core.management.base import BaseCommand, CommandError
from locations.models import Location, LocationHours, MenuItem

#opencage_key = 'b616d3500766a1b29666d5d889b26b48'
#opencage_geocoder = OpenCageGeocode(opencage_key)
gmaps = googlemaps.Client(key='AIzaSyDNMJJB6yqrcOuZlPFPGU7l01hcMTVtDa8')

class Command(BaseCommand):
    help = 'Scrapes food related businesses from Yelp'

    def get_vpn(self, the_vpn, increment):
        vpns = ['US California', 'US East', 'US Florida', 'US Midwest', 'US New York City', 'US Seattle', 'US Silicon Valley', 'US Texas', 'US West']
        current_vpn = 8
        if the_vpn:
            current_vpn = vpns.index(the_vpn)

        if increment:
            if current_vpn == 8:
                current_vpn = 0
            else:
                current_vpn += 1

        return vpns[current_vpn]

    def open_vpn(self, current_vpn):
        connect_string_first = 'osascript<<END\ntell application "Tunnelblick" \n\tconnect "'
        connect_string_second = '" \n\tget state of first configuration where name = "'
        connect_string_third = '" \n\trepeat until result = "CONNECTED" \n\t\tdelay 1 \n\t\tget state of first configuration where name = "'
        connect_string_fourth = '" \n\tend repeat \nend tell \nEND'

        connect = connect_string_first + current_vpn + connect_string_second + current_vpn + connect_string_third + current_vpn + connect_string_fourth
        connected = os.system(connect)
        print connected

    def close_vpn(self, current_vpn):
        disconnect_string_first = 'osascript<<END\ntell application "Tunnelblick" \n\tdisconnect "'
        disconnect_string_second = '" \n\tget state of first configuration where name = "'
        disconnect_string_third = '" \n\trepeat until result = "EXITING" \n\t\tdelay 1 \n\t\tget state of first configuration where name = "'
        disconnect_string_fourth = '" \n\tend repeat \nend tell \nEND'

        disconnect = disconnect_string_first + current_vpn + disconnect_string_second + current_vpn + disconnect_string_third + current_vpn + disconnect_string_fourth
        disconnected = os.system(disconnect)
        print disconnected


    def slugify(self, text):
        """Generates an ASCII-only slug."""
        result = []
        for word in text:
            word = word.encode('translit/long')
            if word:
                result.append(word)
        return unicode("".join(result))

    def handle(self, *args, **options):
        print "Starting scraper"
        restaurant_links = []
        cities_portland = ['Portland', 'Beaverton', 'Hillsboro', 'Tigard', 'Lake+Oswego', 'Oregon+City', 'West+Linn', 'Milwaukie', 'Damascus', 'Gladstone', 'Clackamas', 'Happy+Valley', 'Fairview', 'Troutdale', 'Boring', 'Gresham', 'Tualatin',  'Vancouver']
        cities_los_angeles = ['Hollywood', 'Studio+City', 'Encino', 'Burbank', 'Santa+Monica', 'Sherman+Oaks', 'Woodland+Hills', 'Pasadena', 'Northridge', 'Calabasas', 'Thousand+Oaks', 'Westlake+Village', 'Agoura+Hills', 'Huntington+Beach', 'Newport+Beach', 'Manhattan+Beach', 'Redondo+Beach', 'Alhambra', 'Anaheim', 'Beverly+Hills', 'Long+Beach', 'North+Hollywood', 'Glendale', 'Chatsworth', 'canoga+park', 'west+hills', 'Simi+Valley', 'Tarzana', 'Malibu', 'Los+Angeles']
        cats = ['restaurants', 'foodtrucks', 'icecream', 'desserts', 'coffee', 'breweries', 'bakeries', 'bars']
        current_vpn = self.get_vpn(None, False)
        
        for city in cities_portland:
            for category in cats:
                #disconnect vpn then connect new vpn
                current_vpn = self.get_vpn(current_vpn, False)
                self.close_vpn(current_vpn)
                current_vpn = self.get_vpn(current_vpn, True)
                self.open_vpn(current_vpn)

                for i in xrange(0, 990, 10):
                    print(city, category, i)
                    restaurants = "http://www.yelp.com/search?find_loc=" + city + ",+OR&start=" + str(i) + "&cflt=" + category
                    if city == "Vancouver":
                        restaurants = "http://www.yelp.com/search?find_loc=" + city + ",+WA&start=" + str(i) + "&cflt=" + category
                    restaurant_response = requests.get(restaurants)
                    restaurant_soup = BeautifulSoup(restaurant_response.content)

                    if "We're sorry, the page of results you requested is unavailable." in restaurant_soup:
                        break
                    the_restaurant_links = restaurant_soup.find_all('a', 'biz-name')
                    for restaurant_link in the_restaurant_links:
                        biz_link = restaurant_link.get('href')
                        if "?" in biz_link:
                            split_biz = biz_link.split("?")
                            biz_link = split_biz[0]
                        try:
                            loc_link = Location.objects.get(link=biz_link)
                        except:
                            loc_link = None
                        if ("/biz/" in restaurant_link.get('href')) and (biz_link not in restaurant_links) and not loc_link:
                            restaurant_links.append(biz_link)
                current_vpn, no_details = self.scrape_biz(current_vpn, restaurant_links)
                thefile = open(os.path.expanduser("~/Documents/grubyea/src/locations/management/commands/not_scraped.txt"), 'w')
                for item in no_details:
                    thefile.write("%s,\n" % item)
                thefile.close()
                print "no details total: " + str(len(no_details))
                restaurant_links = []


    def scrape_biz(self, the_vpn, restaurant_links):
        no_details = []
        gmaps_working = True
        current_vpn = the_vpn
        print len(restaurant_links)
        for i, biz in enumerate(restaurant_links):
            if i % 150 == 0:
                #disconnect vpn then connect new vpn
                current_vpn = self.get_vpn(current_vpn, False)
                self.close_vpn(current_vpn)
                current_vpn = self.get_vpn(current_vpn, True)
                self.open_vpn(current_vpn)

            response = requests.get('http://www.yelp.com' + biz)
            soup = BeautifulSoup(response.content)
            try:
                the_name = soup.find("h1", "biz-page-title").get_text()
                name = self.slugify(the_name.strip())
                street_markup = soup.find("span", {"itemprop": "streetAddress"})
                for linebreak in street_markup.find_all('br'):
                    linebreak.replace_with(" " + linebreak.get_text())
                street = self.slugify(street_markup.get_text())
                city = self.slugify(soup.find("span", {"itemprop": "addressLocality"}).get_text())
                state = soup.find("span", {"itemprop": "addressRegion"}).get_text()
                postal = soup.find("span", {"itemprop": "postalCode"}).get_text()
            except:
                print "no name, street, city, state, postal"
                no_details.append(biz)
                continue

            print("%d got location" %i)
            if ("Grocery" or "Supermarket" or "Super Market" or "Walmart" or "Convenience") in name:
                print "grocery or super market in name, CONTINUING"
                continue
            try:
                phone = soup.find("span", "biz-phone").get_text()
                phone = ''.join(phone.split())
            except:
                phone = None
            try:
                website = soup.find("span", "biz-website").find("a").get_text()
            except:
                website = None

            price_range = 1
            try:
                soup_price = soup.find("span", "business-attribute price-range").get_text()
                if soup_price == "$$$$":
                    price_range = 4
                elif soup_price == "$$$":
                    price_range = 3
                elif soup_price == "$$":
                    price_range = 2
            except:
                print "price range not available"
                pass

            try:
                categories = soup.find("span", "category-str-list").find_all("a")
            except Exception, e:
                categories = None

            try:
                menu_link = soup.find("a", "menu-explore").get('href')
            except:
                menu_link = None

            try:
                external_menu_link = soup.find("a", "external-menu").get('href')
            except:
                external_menu_link = None
            
            try:
                hours_table_rows = soup.find("table", "table table-simple hours-table").find_all("tr")
            except:
                hours_table_rows = None

            hours = []
            if hours_table_rows:
                for row in hours_table_rows:
                    these_hours = {"day": "", "open": "", "close": ""}
                    day = row.find("th", {"scope": "row"}).get_text()
                    if day == 'Mon':
                        these_hours["day"] = 1
                    if day == 'Tue':
                        these_hours["day"] = 2
                    if day == 'Wed':
                        these_hours["day"] = 3
                    if day == 'Thu':
                        these_hours["day"] = 4
                    if day == 'Fri':
                        these_hours["day"] = 5
                    if day == 'Sat':
                        these_hours["day"] = 6
                    if day == 'Sun':
                        these_hours["day"] = 7

                    try:
                        times = row.find_all("span", "nowrap")
                    except:
                        times = None
                    if times:
                        for i, time in enumerate(times):
                            if i == 0:
                                these_hours["open"] = time.get_text()
                            elif i == 1:
                                these_hours["close"] = time.get_text()
                    else:
                        these_hours["open"] = "Closed"
                        these_hours["close"] = "Closed"
                    hours.append(these_hours)

            items = []
            if menu_link:
                menu_response = requests.get("http://www.yelp.com" + menu_link)
                menu_soup = BeautifulSoup(menu_response.content)
                try:
                    item_block = menu_soup.find_all("div", "media-block menu-item")
                except:
                    item_block = None

                if item_block:
                    for item in item_block:
                        item_details = {"name": "", "price": "", "description": ""}
                        try:
                            item_details["name"] = self.slugify(item.find("h4").find("a").get_text())
                        except:
                            pass

                        try:
                            menu_price = item.find_all("li", "menu-item-price-amount")[-1].get_text()
                            item_details["price"] = ' '.join(menu_price.split())
                        except:
                            pass

                        try:
                            item_details["description"] = self.slugify(item.find("p", "menu-item-details-description").get_text())
                        except:
                            pass

                        if item_details["name"] != "":
                            items.append(item_details)
            
            full_address = " ".join([street, city, state, postal])
            geo_location = None

            if gmaps_working:
                try:
                    geocode_result = gmaps.geocode(full_address)
                    geo_location = geocode_result[0]["geometry"]["location"]
                except Exception, e:
                    print "Google unable to geocode " + full_address
                    gmaps_working = False
                    geo_location = None
                    break

            location = Location.objects.create(name=name, address=street, city=city, state=state, zip_code=postal, phone=phone, website=website, menu_internal=menu_link, menu_external=external_menu_link, longitude=geo_location['lng'], latitude=geo_location['lat'], price=price_range, link=biz)

            if categories:
                for category in categories:
                    the_category = category.get_text()
                    location.tag.get_or_create(tag=the_category)
            
            location.save()   

            if hours:
                for daily_hours in hours:
                    location_hours = LocationHours.objects.create(location=location, day=daily_hours["day"], open_time=daily_hours["open"], close_time=daily_hours["close"])

            if items:
                for item in items:
                    item_price = None
                    if item['price']:
                        item_price = Decimal((item['price']).strip('$'))

                    menu_item = MenuItem.objects.create(location=location, name=item["name"], description=item["description"], price=item_price)


            #print phone, name, street, city, state, postal, website, menu_link, hours, items
            self.stdout.write(self.style.SUCCESS('Successfully added location %s' % location.name))

        return current_vpn, no_details