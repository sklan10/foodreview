import re, requests, urllib2, json
from decimal import *

from PIL import Image
from io import StringIO, BytesIO

import googlemaps
from bs4 import BeautifulSoup

from django.core.management.base import BaseCommand, CommandError
from locations.models import Location, LocationHours, MenuItem, LocationImage


key='AIzaSyDNMJJB6yqrcOuZlPFPGU7l01hcMTVtDa8'

class Command(BaseCommand):
    help = 'Scrapes images from Google'

    def handle(self, *args, **options):
        print "Starting Google scraper"
        
        for location in Location.objects.all():
            print location.name
            if LocationImage.objects.filter(location=location).count() > 0:
                print "Already images"
                continue
            name = (location.name).replace(" ", "%20")
            keyword_str = str(location.address) + '%20' + str(location.city) + '%20' + str(location.state) + '%20' + str(location.zip_code)
            keyword = keyword_str.replace(" ", "%20")
            requests_str = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' + str(location.latitude) + ',' + str(location.longitude) + '&rankby=distance&name=' + name + '&key=' + key
            response = requests.get(requests_str)
            response_object = response.json()
            try:
                place_id = response_object['results'][0]['place_id']
            except:
                print "No Place ID"
                continue

            place_details = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=' + place_id + '&key=' + key
            place_response = requests.get(place_details)
            place_response = place_response.json()

            photo_references = []
            try:
                photos =place_response["result"]["photos"]
            except Exception, e:
                print "No Photos"
                continue

            for pic in photos:
                html_attributions = []
                for attr in pic["html_attributions"]:
                    html_attributions.append(attr)
                photo_references.append({'pic': pic["photo_reference"], 'attributions': html_attributions})

            added_images = False
            for photo_reference in photo_references:
                photo_str = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=1600&photoreference=' + photo_reference['pic'] + '&key=' + key
                attributions = ",".join(photo_reference['attributions'])

                try:
                    image_already = LocationImage.objects.get(location=location, image_str=photo_str)
                except Exception, e:
                    image_already = False

                if not image_already:
                    location_image = LocationImage.objects.create(location=location, image_str=photo_str, attributions=attributions)
                    added_images = True
                    
            print "SUCCESSFULLY ADDED LOCATION IMAGES"

        