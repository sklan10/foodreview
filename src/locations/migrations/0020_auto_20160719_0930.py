# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-19 16:30
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0019_locationreview_featured'),
    ]

    operations = [
        migrations.CreateModel(
            name='LocationImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to='location_image/')),
                ('default', models.BooleanField(default=False)),
                ('featured', models.BooleanField(default=False)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='itemrating',
            name='image',
        ),
        migrations.RemoveField(
            model_name='location',
            name='image',
        ),
        migrations.RemoveField(
            model_name='locationreview',
            name='image',
        ),
        migrations.AddField(
            model_name='itemrating',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2016, 7, 19, 16, 28, 57, 256085, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='itemrating',
            name='updated',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2016, 7, 19, 16, 29, 6, 807281, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2016, 7, 19, 16, 29, 11, 630986, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='location',
            name='updated',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2016, 7, 19, 16, 29, 15, 638744, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='locationhours',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2016, 7, 19, 16, 29, 22, 782310, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='locationhours',
            name='updated',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2016, 7, 19, 16, 29, 27, 461806, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='locationreview',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2016, 7, 19, 16, 29, 30, 965813, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='locationreview',
            name='updated',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2016, 7, 19, 16, 29, 34, 581492, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='locationtag',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2016, 7, 19, 16, 29, 41, 133081, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='locationtag',
            name='updated',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2016, 7, 19, 16, 29, 46, 780766, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='menuitem',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2016, 7, 19, 16, 29, 50, 788510, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='menuitem',
            name='updated',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2016, 7, 19, 16, 29, 54, 492380, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='menuitemcategory',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2016, 7, 19, 16, 29, 57, 820171, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='menuitemcategory',
            name='updated',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2016, 7, 19, 16, 30, 0, 891813, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='locationimage',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='locations.Location'),
        ),
        migrations.AddField(
            model_name='locationimage',
            name='rating',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='locations.ItemRating'),
        ),
        migrations.AddField(
            model_name='locationimage',
            name='review',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='locations.LocationReview'),
        ),
    ]
