# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-29 17:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0023_favoritelocation'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationimage',
            name='image_string',
            field=models.TextField(blank=True, null=True),
        ),
    ]
