# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-07 19:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0005_itemrating_location'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='slug',
            field=models.SlugField(unique=True),
        ),
    ]
