from __future__ import unicode_literals

from datetime import datetime, date, timedelta

from django.db import models
from django.db.models import Q
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in
from django.core.urlresolvers import reverse
from django.utils import timezone

from autoslug import AutoSlugField
from geopy.geocoders import Nominatim
from geopy.distance import vincenty



class LocationManager(models.Manager):
    def find_nearby(self, location_position, radius, prices, name_or_category):
		ids = []
		for loc in Location.objects.all():
			if float(vincenty(location_position, (loc.latitude, loc.longitude)).miles) <= float(radius) and loc.id not in ids:
				ids.append(loc.id)

		price_query = Q()
		for price in prices:
			price_query |= Q(price__exact=int(price))

		name_or_category_query = Q(tag__tag__icontains=name_or_category) | Q(name__icontains=name_or_category)

		return self.filter(id__in=ids).filter(price_query, name_or_category_query).distinct().order_by('-featured', 'id')


class LocationTag(models.Model):
	tag = models.CharField(max_length=200)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __unicode__(self):
		return self.tag

FEATURED = (
    (1, 1),
    (2, 2),
    (3, 3),
)

class Location(models.Model):
	name = models.CharField(max_length=120)
	address = models.TextField()
	city = models.CharField(max_length=120)
	state = models.CharField(max_length=2)
	zip_code = models.CharField(max_length=12)
	phone = models.CharField(max_length=13, null=True, blank=True)
	website = models.TextField(null=True, blank=True)
	menu_internal = models.TextField(null=True, blank=True)
	menu_external = models.TextField(null=True, blank=True)
	longitude = models.FloatField()
	latitude = models.FloatField()
	slug = AutoSlugField(populate_from='name', unique=True, editable=True)
	tag = models.ManyToManyField(LocationTag, blank=True)
	price = models.IntegerField(default=1)
	link = models.TextField(null=True, blank=True)
	featured = models.IntegerField(default=1, choices=FEATURED)
	food_rating = models.IntegerField(default=100)
	restaurant_rating = models.IntegerField(default=100)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)
	objects = LocationManager()

	def __unicode__(self):
		return u'%s' % self.name

	def get_absolute_url(self):
		return "/location/%s/" % self.slug

DAYS_OF_WEEK = (
    ("1", 'Mon'),
    ("2", 'Tue'),
    ("3", 'Wed'),
    ("4", 'Thu'),
    ("5", 'Fri'),
    ("6", 'Sat'),
    ("7", 'Sun'),
)

class LocationHours(models.Model):
	location = models.ForeignKey(Location)
	day = models.CharField(max_length=12, choices=DAYS_OF_WEEK)
	open_time = models.CharField(max_length=10)
	close_time = models.CharField(max_length=10)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __unicode__(self):
		return unicode(self.location)


class MenuItemCategory(models.Model):
	category = models.CharField(max_length=200)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __unicode__(self):
		return self.tag


class MenuItem(models.Model):
	location = models.ForeignKey(Location)
	name = models.CharField(max_length=120)
	description = models.TextField(null=True, blank=True)
	price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
	category = models.ForeignKey(MenuItemCategory, null=True, blank=True)
	image = models.ImageField(upload_to='item_image/', null=True, blank=True)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __unicode__(self):
		return unicode(self.location)


class ItemRating(models.Model):
	location = models.ForeignKey(Location)
	user = models.ForeignKey(User)
	item = models.ForeignKey(MenuItem, null=True, blank=True)
	name = models.CharField(max_length=140)
	description = models.CharField(max_length=140)
	rating = models.BooleanField(default=True)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __unicode__(self):
		return unicode(self.location)


class LocationReview(models.Model):
	location = models.ForeignKey(Location)
	user = models.ForeignKey(User)
	review = models.TextField()
	featured = models.BooleanField(default=False)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __unicode__(self):
		return unicode(self.location)



class LocationImage(models.Model):
	location = models.ForeignKey(Location)
	user = models.ForeignKey(User, null=True, blank=True)
	review = models.ForeignKey(LocationReview, null=True, blank=True)
	image = models.ImageField(upload_to='location_image/', null=True, blank=True)
	image_str = models.TextField(null=True, blank=True)
	attributions = models.TextField(null=True, blank=True)
	default = models.BooleanField(default=False)
	featured = models.BooleanField(default=False)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __unicode__(self):
		return str(self.location)


class FavoriteLocation(models.Model):
	user = models.ForeignKey(User)
	location = models.ForeignKey(Location)
	active = models.BooleanField(default=True)
	timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __unicode__(self):
		return str(self.user)

	class Meta:
		ordering = ['-timestamp']

	def get_absolute_url(self):
		return "/location/%s/" % self.location.slug