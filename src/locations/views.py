import json

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives, EmailMessage
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect 
from django.shortcuts import render, render_to_response, HttpResponseRedirect, RequestContext, Http404, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from django.core import mail
connection = mail.get_connection()

import googlemaps
from geopy.geocoders import Nominatim
from geopy.distance import vincenty
#from opencage.geocoder import OpenCageGeocode

from .models import Location, LocationHours, MenuItem, ItemRating, LocationReview, LocationImage, FavoriteLocation

#opencage_key = 'b616d3500766a1b29666d5d889b26b48'
#opencage_geocoder = OpenCageGeocode(opencage_key)
gmaps = googlemaps.Client(key='AIzaSyDNMJJB6yqrcOuZlPFPGU7l01hcMTVtDa8')


def home(request):
	return render_to_response("home.html", locals(), context_instance=RequestContext(request))


def search(request):
	try:
		get_loc = request.GET.get('location', '')
	except:
		get_loc = False
	if not get_loc:
		return HttpResponseRedirect('/search/?location=97205&radius=5')

	try:
		name_or_category = request.GET.get('name_or_category', '')
	except:
		name_or_category = False
	try:
		get_radius = request.GET.get('radius', '')
	except:
		get_radius = 5
	if not get_radius or get_radius == '':
		get_radius = 5

	try:
		prices = request.GET.getlist('price', '')
	except:
		prices = False
		
	try:
		geocode_result = gmaps.geocode(get_loc)
		geo_location = geocode_result[0]["geometry"]["location"]
		latitude, longitude = geo_location['lat'], geo_location['lng']
		location_position = (geo_location['lat'], geo_location['lng'])
	except:
		messages.warning(request, "There was a problem locating your address. Please try again using your full address or only your zipcode.")
		return HttpResponseRedirect(reverse('search'))

	searched_locations = Location.objects.find_nearby(location_position, get_radius, prices, name_or_category)
	paginator = Paginator(searched_locations, 25)
	page = request.GET.get('page')
	try:
	    locations = paginator.page(page)
	except PageNotAnInteger:
	    # If page is not an integer, deliver first page.
	    locations = paginator.page(1)
	except EmptyPage:
	    # If page is out of range (e.g. 9999), deliver last page of results.
	    locations = paginator.page(paginator.num_pages)

	return render_to_response("search.html", locals(), context_instance=RequestContext(request))	


def location(request, slug):
	location = get_object_or_404(Location, slug=slug)
	reviews = LocationReview.objects.filter(location=location)
	images = LocationImage.objects.filter(location=location).order_by('-featured')
	ratings = ItemRating.objects.filter(location=location).order_by('-rating')

	the_ratings = []
	rating_descriptions = []
	for rating in ratings:
		already_a_rating = False
		for the_rating in the_ratings:
			if the_rating['name'] == rating.name:
				already_a_rating = True
				the_rating['count'] += 1
				if rating.rating:
					the_rating['total_thumbs_up'] += 1
				else:
					the_rating['total_thumbs_down'] += 1
				the_rating['average'] = int(round((float(the_rating['total_thumbs_up']) / float(the_rating['count'])) * 100))

		if not already_a_rating:
			description = None
			if rating.item:
				description = rating.item.description
			the_rating = {'name': rating.name, 'description': description, 'count': 1, 'total_thumbs_up': 0, 'total_thumbs_down': 0, 'average': 0}
			if rating.rating:
				the_rating['total_thumbs_up'] += 1
				the_rating['average'] = 100
			else:
				the_rating['total_thumbs_down'] += 1

			the_ratings.append(the_rating)

		if rating.description and rating.description != "" and rating.description != None:
			counter = 0
			for rating_description in rating_descriptions:
				if rating_description.name == rating.name:
					counter += 1
			if counter < 5:
				rating_descriptions.append(rating)

	#sort the list top to bottom
	the_ratings = sorted(the_ratings, key=lambda k: k['average'], reverse=True) 
	ratings_count = len(the_ratings)
	ratings_half = (ratings_count / 2) + 1

	try:
		location_hours = LocationHours.objects.filter(location=location).order_by('day')
	except:
		location_hours = None
	try:
		favorite = FavoriteLocation.objects.get(location=location, user=request.user, active=True)
	except:
		favorite = None

	return render_to_response("location.html", locals(), context_instance=RequestContext(request))


@login_required
def write_review(request, slug):
	location = get_object_or_404(Location, slug=slug)
	first_other = (MenuItem.objects.filter(location=location).count()) + 1
	try:
		new_review = LocationReview.objects.get(location=location, user=request.user)
	except Exception, e:
		new_review = None

	if request.method == "POST":
		ratings = request.POST["all_data"]
		images = request.FILES.getlist("the-image")
		restaurant_review = request.POST["restaurant_review"]

		if new_review:
			new_review.review = restaurant_review
			new_review.save()
		else:
			total_reviews = LocationReview.objects.filter(location=location).count()
			if total_reviews < 3:
				new_review = LocationReview.objects.create(location=location, user=request.user, review=restaurant_review, featured=True)
			else:
				new_review = LocationReview.objects.create(location=location, user=request.user, review=restaurant_review)


		the_ratings = json.loads(ratings)
		for rating in the_ratings:
			try:
				item = MenuItem.objects.get(location=location, name=rating['name'])
			except Exception, e:
				item = None

			thumbs = True
			if rating['good'] == "2":
				thumbs = False

			if item:
				new_rating = ItemRating.objects.create(location=location, user=request.user, item=item, name=item.name, description=rating['description'], rating=thumbs)
			else:
				new_rating = ItemRating.objects.create(location=location, user=request.user, name=rating['name'], description=rating['description'], rating=thumbs)

		for image in images:
			new_image = LocationImage.objects.create(location=location, user=request.user, review=new_review, image=image)

		messages.warning(request, "Your review and item ratings have been saved. We will review your submission for consideration as a featured review. Thanks!")
		return HttpResponseRedirect(reverse('location', kwargs={'slug':location.slug}))
	return render_to_response("rate_review.html", locals(), context_instance=RequestContext(request))


@login_required
def add_favorite(request, slug):
	location = get_object_or_404(Location, slug=slug)
	favorite, created = FavoriteLocation.objects.get_or_create(user=request.user, location=location)
	message = "Favorite saved"
	if not created:
		if favorite.active == True:
			favorite.active = False
			favorite.save()
			message = "Favorite updated"
		else:
			favorite.active = True
			favorite.save()

	messages.warning(request, message)
	return HttpResponseRedirect(reverse("location", kwargs={'slug': location.slug}))
