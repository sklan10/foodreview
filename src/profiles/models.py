from __future__ import unicode_literals

from datetime import datetime, date, timedelta

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in
from django.core.urlresolvers import reverse
from django.utils import timezone



class UserProfile(models.Model):
	user = models.OneToOneField(User)
	location = models.TextField(null=True, blank=True)
	description = models.TextField(null=True, blank=True)
	image = models.ImageField(upload_to='profile_image/', null=True, blank=True)
	activation_key = models.CharField(max_length=40, null=True, blank=True)
	key_expires = models.DateTimeField(null=True, blank=True)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __unicode__(self):
		return str(self.user)