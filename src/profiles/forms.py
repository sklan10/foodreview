from django import forms
from django.contrib.auth.models import User

from .models import UserProfile

class LoginForm(forms.Form):
	email = forms.CharField(label="Username or Email")
	password = forms.CharField(widget=forms.PasswordInput)

class RegistrationForm(forms.Form):
	username = forms.CharField()
	email = forms.EmailField()
	password = forms.CharField(widget=forms.PasswordInput)
	password_verify = forms.CharField(label = "retype password", widget=forms.PasswordInput)

	def clean_username(self):
		username = self.cleaned_data['username']
		if User.objects.filter(username=username).exists():
			raise forms.ValidationError("This user name already exist. Did you forget your password?")
		return username

	def clean_email(self):
		email = self.cleaned_data['email']
		if User.objects.filter(email=email).exists():
			raise forms.ValidationError("This email already exist. Did you forget your password?")
		return email

	def clean(self):
		cleaned_data = super(RegistrationForm, self).clean()
		password = cleaned_data.get("password")
		password_verify = cleaned_data.get("password_verify")
		if password != password_verify:
			self._errors["password"] = self.error_class(["Password does not match password confirmation"])
			self._errors["password_verify"] = self.error_class(["Confirmation does not match password"])
			raise forms.ValidationError("passwords do not match")
			del cleaned_data['password']
			del cleaned_data['password_verify']
		return cleaned_data


class ProfileForm(forms.Form):
	username = forms.CharField()
	email = forms.EmailField()
	location = forms.CharField(required=False)
	about_them = forms.CharField(required=False)
	the_image = forms.ImageField(required=False)

	def __init__(self, *args, **kwargs):
		self.user = kwargs.pop('user')
		super(ProfileForm, self).__init__(*args, **kwargs)

	def clean_username(self):
		username = (self.cleaned_data['username']).lower()
		if User.objects.filter(username=username).exists():
			if not self.user.username == username:
				raise forms.ValidationError("This user name already exist.")
		return username

	def clean_email(self):
		email = (self.cleaned_data['email']).lower()
		if User.objects.filter(email=email).exists():
			if not self.user.email == email:
				raise forms.ValidationError("This email already exist.")
		return email

