import hashlib, random

from datetime import datetime, timedelta
from django.utils import timezone

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives, EmailMessage
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response, HttpResponseRedirect, RequestContext, Http404, get_object_or_404
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt

from django.core import mail
connection = mail.get_connection()

from .forms import LoginForm, RegistrationForm, ProfileForm
from .models import UserProfile
from locations.models import Location, LocationHours, MenuItem, ItemRating, LocationReview, LocationImage, FavoriteLocation

def user_login(request):
	form = LoginForm(request.POST or None)
	next_page = ""
	if request.GET:  
		next_page = request.GET['next']

	if form.is_valid():
		username_email = form.cleaned_data['email']
		password = form.cleaned_data['password']
		try:
		    the_user = User.objects.get(username=username_email)
		except User.DoesNotExist:
		    the_user = User.objects.filter(email=username_email)
		    if len(the_user) == 1:
		        the_user = the_user[0]
		    else:
		        the_user = None

		if the_user is not None:
		    user = authenticate(username=the_user.username, password=password)
		    if user is not None:
		        if user.is_active:
		            login(request, user)
		            if next_page:
		            	return HttpResponseRedirect(next_page)
		            return HttpResponseRedirect(reverse('home'))
		        else:
		        	messages.warning(request, "Your account is not active")
		        	if next_page:
		        		return HttpResponseRedirect(next_page)
		        	return HttpResponseRedirect(reverse('home'))
		    else:
		        messages.warning(request, "Incorrect Password")
		        if next_page:
		        	return HttpResponseRedirect(next_page)
		        return HttpResponseRedirect(reverse('home'))
		else:
		    messages.warning(request, "The username or email is incorrect")
		    if next_page:
		    	return HttpResponseRedirect(next_page)
		    return HttpResponseRedirect(reverse('home'))

	elif form.errors:
		values = []
		if form['email'].errors:
			for error in form['email'].errors:
				values.append("Email: " + error)
		if form['password'].errors:
			for error in form['password'].errors:
				values.append("Password: " + error)

		the_errors = " ".join(values)
		messages.warning(request, the_errors)
		if next_page:
			return HttpResponseRedirect(next_page)
		return HttpResponseRedirect(reverse('home'))


def user_register(request):
    form = RegistrationForm(request.POST or None)
    next_page = ""
    if request.GET:  
		next_page = request.GET['next']
    if form.is_valid():
		username = form.cleaned_data['username']
		email = form.cleaned_data['email']
		password = form.cleaned_data['password']
		new_user = User.objects.create_user(username, email, password)
		new_user.is_active = False
		new_user.save()

		salt = hashlib.sha1(str(random.random())).hexdigest()[:5]            
		activation_key = hashlib.sha1(salt+new_user.email).hexdigest()            
		key_expires = timezone.now() + timedelta(2)

		#Create User Profile
		user_profile = UserProfile.objects.create(user=new_user, activation_key=activation_key, key_expires=key_expires)

		#send activation email
		site = "https://www.marckie.com/"
		ctx_dict = {'activation_key': user_profile.activation_key, 'site': site}
		message_html = render_to_string('emails/new_user.html', ctx_dict)
		msg = mail.EmailMessage("GrubYea.com: Activate Your Account!", message_html, settings.DEFAULT_FROM_EMAIL, [email])
		msg.content_subtype = "html"
		msg.send()

		messages.warning(request, "Please check your email to activate your account.")
		if next_page:
			return HttpResponseRedirect(next_page)
		return HttpResponseRedirect(reverse('home'))

    elif form.errors:
		values = []
		if form['username'].errors:
			for error in form['username'].errors:
				values.append(error)
		if form['email'].errors:
			for error in form['email'].errors:
				values.append(error)
		if form['password'].errors:
			for error in form['password'].errors:
				values.append(error)
		if form['password_verify'].errors:
			for error in form['password_verify'].errors:
				values.append(error)

		the_errors = " ".join(values)
		messages.warning(request, the_errors)
		if next_page:
			return HttpResponseRedirect(next_page)
		return HttpResponseRedirect(reverse('home'))


def register_confirm(request, activation_key):
    #if logged in redirect 
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('home'))

    #find StoreProfile matches activation key
    user_profile = get_object_or_404(UserProfile, activation_key=activation_key)

    #check if the activation key has expired, if it hase then render confirm_expired.html
    if user_profile.key_expires < timezone.now():
        return render_to_response('registration/confirm_expired.html')
    
    #if the key hasn't expired set active
    user = user_profile.user
    user.is_active = True
    user.save()
    return render_to_response('registration/confirm.html')


def user_logout(request):
    logout(request)
    next_page = ""
    if request.GET:
    	next_page = request.GET['next']

	messages.warning(request, "Successfully logged out.")
	#if next_page:
		#return HttpResponseRedirect(next_page)
    return HttpResponseRedirect(reverse('home'))


@login_required
def profile(request):
	profile_user = get_object_or_404(UserProfile, user=request.user)
	favorites = FavoriteLocation.objects.filter(user=profile_user.user)
	ratings = ItemRating.objects.filter(user=profile_user.user)
	reviews = LocationReview.objects.filter(user=profile_user.user)
	images = LocationImage.objects.filter(user=profile_user.user)

	group_locations = []
	for review in reviews:
		group_locations.append({'location': review.location.name, 'slug': review.location.slug, 'review': review.review, 'featured': review.featured, 'timestamp': review.timestamp, 'ratings':[], 'images':[]})

	for rating in ratings:
		location_already = False
		for location in group_locations:
			if rating.location.name == location['location']:
				location_already = True
				location['ratings'].append({'name': rating.name, 'description': rating.description, 'rating': rating.rating, 'timestamp': rating.timestamp})

	for image in images:
		location_already = False
		for location in group_locations:
			if image.location.name == location['location']:
				location_already = True
				location['images'].append({'image': image.image, 'featured': image.featured})
	

	paginator = Paginator(group_locations, 10)
	page = request.GET.get('page')
	try:
	    locations = paginator.page(page)
	except PageNotAnInteger:
	    # If page is not an integer, deliver first page.
	    locations = paginator.page(1)
	except EmptyPage:
	    # If page is out of range (e.g. 9999), deliver last page of results.
	    locations = paginator.page(paginator.num_pages)		

	profile_form = ProfileForm(request.POST or None, request.FILES or None, user=request.user)
	if request.method == "POST":
		if profile_form.is_valid():
			profile_user.user.username = profile_form.cleaned_data['username']
			profile_user.user.email = profile_form.cleaned_data['email']
			profile_user.location = profile_form.cleaned_data['location']
			profile_user.description = profile_form.cleaned_data['about_them']
			profile_user.image = profile_form.cleaned_data['the_image']
			profile_user.save()

			messages.warning(request, 'Your profile has been saved.')
			return HttpResponseRedirect(reverse(profile))
	return render_to_response("profile.html", locals(), context_instance=RequestContext(request))